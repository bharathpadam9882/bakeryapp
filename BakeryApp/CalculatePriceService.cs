﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryApp
{
    public class CalculatePriceService
    {
        Pack packObj = new Pack();
        public double CalculateTotalPrice(List<Order> orders)
        {
            double totalPrice = 0;
            foreach(Order order in orders)
            {
                totalPrice = totalPrice + CalculatePriceForIndividualItem(order);
            }

            return totalPrice;
        }

        private double CalculatePriceForIndividualItem(Order order)
        {
            List<Pack> packs = packObj.GetPacksByProductCode(order.ProductCode);
            List<int> packSizeList = packs.Select(x => x.Units).OrderByDescending(x => x).ToList();

            IDictionary<int, int> output = new Dictionary<int, int>();

            int q = order.Quantity;
            int start = 0;
            int packSize = 0;

            while (q > 0 && start < packSizeList.Count)
            {
                if (packSize > 0)
                {
                    if (packSizeList.IndexOf(packSize) + 1 == packSizeList.Count)
                    {
                        packSize = packSizeList[0];
                    }

                    if (output.ContainsKey(packSize))
                    {
                        q = q + packSize;

                        if (output[packSize] > 1)
                        {
                            output[packSize] = output[packSize] - 1;
                        }
                        else
                        {
                            output.Remove(packSize);
                        }

                        start = packSizeList.IndexOf(packSize) + 1;
                    }
                }

                for (int i = start; i < packSizeList.Count; i++)
                {
                    if (q / packSizeList[i] > 0)
                    {
                        packSize = packSizeList[i];
                        output.Add(packSize, q / packSize);
                        q = q % packSize;
                    }
                }

                start++;
            }

            if (q > 0)
            {
                output.Clear();
            }
            double price = 0;
            if(q > 0)
            {
                throw new Exception("Unknown error occured while caculating price");
            }
            foreach(KeyValuePair<int, int> keyValuePair in output)
            {
                price = price + packs.FirstOrDefault(x => x.Units == keyValuePair.Key).Price * keyValuePair.Value;
            }
            return price;
        }
    }
}
