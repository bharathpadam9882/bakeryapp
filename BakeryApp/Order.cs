﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryApp
{
    public class Order
    {
        public string ProductCode { get; set; }
        public int Quantity { get; set; }

        public List<Order> GetFirstOrder()
        {
            List<Order> orders = new List<Order>();

            orders.Add(new Order() { ProductCode = "VS5", Quantity = 10 });
            orders.Add(new Order() { ProductCode = "MB11", Quantity = 14 });
            orders.Add(new Order() { ProductCode = "CF", Quantity = 13 });

            return orders;
        }

        public List<Order> GetSecondOrder()
        {
            List<Order> orders = new List<Order>();

            orders.Add(new Order() { ProductCode = "VS5", Quantity = 30 });
            orders.Add(new Order() { ProductCode = "MB11", Quantity = 10 });
            orders.Add(new Order() { ProductCode = "CF", Quantity = 15 });

            return orders;
        }
    }
}
