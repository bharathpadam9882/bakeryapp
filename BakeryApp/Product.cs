﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryApp
{
    public class Product
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }

        public Product()
        {
        }

        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();

            products.Add(new Product() { ProductName = "Vegemite Scroll", ProductCode = "VS5"});
            products.Add(new Product() { ProductName = "Blueberry Muffin", ProductCode = "MB11" });
            products.Add(new Product() { ProductName = "Croissant", ProductCode = "CF" });

            return products;
        }


    }
}
