﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryApp
{
    public class Pack
    {
        public int Units { get; set; }
        public string ProductCode { get; set; }
        public double Price { get; set; }

        private List<Pack> GetPacks()
        {
            List<Pack> packs = new List<Pack>();

            packs.Add(new Pack() { Units = 3, ProductCode ="VS5", Price = 6.99 });
            packs.Add(new Pack() { Units = 5, ProductCode = "VS5", Price = 8.99 });
            packs.Add(new Pack() { Units = 2, ProductCode = "MB11", Price = 9.95 });
            packs.Add(new Pack() { Units = 5, ProductCode = "MB11", Price = 16.95 });
            packs.Add(new Pack() { Units = 8, ProductCode = "MB11", Price = 24.95 });
            packs.Add(new Pack() { Units = 3, ProductCode = "CF", Price = 5.95 });
            packs.Add(new Pack() { Units = 5, ProductCode = "CF", Price = 9.95 });
            packs.Add(new Pack() { Units = 9, ProductCode = "CF", Price = 16.99 });

            return packs;
        }

        public List<Pack> GetPacksByProductCode(string productCode)
        {
            var packs = GetPacks();

            return packs.Where(x =>x.ProductCode == productCode).ToList();
        }
    }
}
