﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            CalculatePriceService calculatePriceService = new CalculatePriceService();
            Order order = new Order();
            var orders = order.GetSecondOrder();

            double totalPrice = calculatePriceService.CalculateTotalPrice(orders);
            Console.WriteLine("Total Price: " + totalPrice);
            Console.ReadLine();
        }
    }
}
